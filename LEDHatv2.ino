
#include "FastLED.h"

#define NUM_LEDS 300

typedef struct {
  byte space;
  byte size;
} ascii;

typedef struct {
  byte r;
  byte g;
  byte b;
} s_color;

/*
 *  80   81  82      83  84
 * 114  115 116     117 118
 * 148  149 150     151 152
 * 183  184 185     186 187
 * 218  219 220     221 222
 */

const byte ascii_20[] PROGMEM = {2, 0}; // SPACE
const byte ascii_21[] PROGMEM = {0, 0}; // ! NOT IMPLEMENTED YET
const byte ascii_22[] PROGMEM = {0, 0}; // " NOT IMPLEMENTED YET
const byte ascii_23[] PROGMEM = {0, 0}; // # NOT IMPLEMENTED YET
const byte ascii_24[] PROGMEM = {0, 0}; // $ NOT IMPLEMENTED YET
const byte ascii_25[] PROGMEM = {0, 0}; // % NOT IMPLEMENTED YET
const byte ascii_26[] PROGMEM = {0, 0}; // & NOT IMPLEMENTED YET
const byte ascii_27[] PROGMEM = {0, 0}; // ' NOT IMPLEMENTED YET
const byte ascii_28[] PROGMEM = {0, 0}; // ( NOT IMPLEMENTED YET
const byte ascii_29[] PROGMEM = {0, 0}; // ) NOT IMPLEMENTED YET
const byte ascii_2a[] PROGMEM = {0, 0}; // * NOT IMPLEMENTED YET
const byte ascii_2b[] PROGMEM = {0, 0}; // + NOT IMPLEMENTED YET
const byte ascii_2c[] PROGMEM = {0, 0}; // , NOT IMPLEMENTED YET
const byte ascii_2d[] PROGMEM = {0, 0}; // - NOT IMPLEMENTED YET
const byte ascii_2e[] PROGMEM = {0, 0}; // . NOT IMPLEMENTED YET
const byte ascii_2f[] PROGMEM = {0, 0}; // / NOT IMPLEMENTED YET
const byte ascii_30[] PROGMEM = {0, 0}; // 0 NOT IMPLEMENTED YET
const byte ascii_31[] PROGMEM = {0, 0}; // 1 NOT IMPLEMENTED YET
const byte ascii_32[] PROGMEM = {0, 0}; // 2 NOT IMPLEMENTED YET
const byte ascii_33[] PROGMEM = {0, 0}; // 3 NOT IMPLEMENTED YET
const byte ascii_34[] PROGMEM = {0, 0}; // 4 NOT IMPLEMENTED YET
const byte ascii_35[] PROGMEM = {0, 0}; // 5 NOT IMPLEMENTED YET
const byte ascii_36[] PROGMEM = {0, 0}; // 6 NOT IMPLEMENTED YET
const byte ascii_37[] PROGMEM = {0, 0}; // 7 NOT IMPLEMENTED YET
const byte ascii_38[] PROGMEM = {0, 0}; // 8 NOT IMPLEMENTED YET
const byte ascii_39[] PROGMEM = {0, 0}; // 9 NOT IMPLEMENTED YET
const byte ascii_3a[] PROGMEM = {0, 0}; // : NOT IMPLEMENTED YET
const byte ascii_3b[] PROGMEM = {0, 0}; // ; NOT IMPLEMENTED YET
const byte ascii_3c[] PROGMEM = {0, 0}; // < NOT IMPLEMENTED YET
const byte ascii_3d[] PROGMEM = {0, 0}; // = NOT IMPLEMENTED YET
const byte ascii_3e[] PROGMEM = {0, 0}; // > NOT IMPLEMENTED YET
const byte ascii_3f[] PROGMEM = {0, 0}; // ? NOT IMPLEMENTED YET
const byte ascii_40[] PROGMEM = {0, 0}; // @ NOT IMPLEMENTED YET
const byte ascii_41[] PROGMEM = {3, 10, 148, 183, 218, 150, 185, 220, 149, 114, 116, 81}; // A
const byte ascii_42[] PROGMEM = {3, 11, 80, 114, 148, 183, 218, 81, 149, 219, 116, 185, 150}; // B 
const byte ascii_43[] PROGMEM = {3, 9, 80, 114, 148, 183, 218, 81, 82, 219, 220}; // C
const byte ascii_44[] PROGMEM = {3, 9, 80, 114, 148, 183, 218, 81, 219, 116, 185}; // D
const byte ascii_45[] PROGMEM = {3, 10, 80, 114, 148, 183, 218, 81, 82, 149, 219, 220}; // E
const byte ascii_46[] PROGMEM = {3, 8, 80, 114, 148, 183, 218, 81, 82, 149}; // F
const byte ascii_47[] PROGMEM = {3, 9, 81, 82, 114, 148, 183, 219, 220, 185, 150}; // G
const byte ascii_48[] PROGMEM = {3, 11, 80, 114, 148, 183, 218, 82, 116, 150, 185, 220, 149}; // H
const byte ascii_49[] PROGMEM = {3, 9, 81, 115, 149, 184, 219, 80, 82, 218, 220}; // I
const byte ascii_4a[] PROGMEM = {3, 6, 82, 116, 150, 185, 219, 183}; // J
const byte ascii_4b[] PROGMEM = {3, 10, 80, 114, 148, 183, 218, 149, 82, 116, 184, 220}; // K
const byte ascii_4c[] PROGMEM = {3, 7, 80, 114, 148, 183, 218, 219, 220}; // L
const byte ascii_4d[] PROGMEM = {4, 13, 80, 114, 148, 183, 218, 115, 150, 116, 83, 117, 151, 186, 221}; // M
const byte ascii_4e[] PROGMEM = {4, 13, 80, 114, 148, 183, 218, 115, 150, 185, 83, 117, 151, 186, 221}; // N
const byte ascii_4f[] PROGMEM = {4, 10, 114, 148, 183, 81, 82, 219, 220, 117, 151, 186}; // O
const byte ascii_50[] PROGMEM = {3, 8, 80, 114, 148, 183, 218, 81, 116, 149}; // P
const byte ascii_51[] PROGMEM = {4, 10, 114, 148, 183, 81, 82, 219, 221, 117, 151, 185}; // Q
const byte ascii_52[] PROGMEM = {3, 10, 80, 114, 148, 183, 218, 81, 116, 149, 185, 220}; // R
const byte ascii_53[] PROGMEM = {3, 9, 81, 82, 114, 148, 149, 150, 185, 218, 219}; // S
const byte ascii_54[] PROGMEM = {3, 7, 80, 81, 82, 115, 149, 184, 219}; // T
const byte ascii_55[] PROGMEM = {4, 10, 80, 114, 148, 183, 219, 220, 186, 151, 117, 83}; // U
const byte ascii_56[] PROGMEM = {3, 9, 80, 114, 148, 183, 219, 82, 116, 150, 185}; // V
const byte ascii_57[] PROGMEM = {4, 13, 80, 114, 148, 183, 218, 184, 150, 185, 83, 117, 151, 186, 221}; // W
const byte ascii_58[] PROGMEM = {3, 10, 80, 114, 183, 218, 82, 116, 185, 220, 149, 150}; // X
const byte ascii_59[] PROGMEM = {3, 8, 80, 114, 184, 219, 82, 116, 149, 150}; // Y
const byte ascii_5a[] PROGMEM = {3, 9, 80, 81, 82, 116, 149, 183, 218, 219, 220}; // Z
const byte ascii_5b[] PROGMEM = {0, 0}; // [ NOT IMPLEMENTED YET
const byte ascii_5c[] PROGMEM = {0, 0}; // \ NOT IMPLEMENTED YET
const byte ascii_5d[] PROGMEM = {0, 0}; // ] NOT IMPLEMENTED YET
const byte ascii_5e[] PROGMEM = {0, 0}; // ^ NOT IMPLEMENTED YET
const byte ascii_5f[] PROGMEM = {0, 0}; // _ NOT IMPLEMENTED YET
const byte ascii_60[] PROGMEM = {0, 0}; // ` NOT IMPLEMENTED YET
const byte ascii_61[] PROGMEM = {0, 0}; // a NOT IMPLEMENTED YET
const byte ascii_62[] PROGMEM = {0, 0}; // b NOT IMPLEMENTED YET
const byte ascii_63[] PROGMEM = {0, 0}; // c NOT IMPLEMENTED YET
const byte ascii_64[] PROGMEM = {0, 0}; // d NOT IMPLEMENTED YET
const byte ascii_65[] PROGMEM = {0, 0}; // e NOT IMPLEMENTED YET
const byte ascii_66[] PROGMEM = {0, 0}; // f NOT IMPLEMENTED YET
const byte ascii_67[] PROGMEM = {0, 0}; // g NOT IMPLEMENTED YET
const byte ascii_68[] PROGMEM = {0, 0}; // h NOT IMPLEMENTED YET
const byte ascii_69[] PROGMEM = {0, 0}; // i NOT IMPLEMENTED YET
const byte ascii_6a[] PROGMEM = {0, 0}; // j NOT IMPLEMENTED YET
const byte ascii_6b[] PROGMEM = {0, 0}; // k NOT IMPLEMENTED YET
const byte ascii_6c[] PROGMEM = {0, 0}; // l NOT IMPLEMENTED YET
const byte ascii_6d[] PROGMEM = {0, 0}; // m NOT IMPLEMENTED YET
const byte ascii_6e[] PROGMEM = {0, 0}; // n NOT IMPLEMENTED YET
const byte ascii_6f[] PROGMEM = {0, 0}; // o NOT IMPLEMENTED YET
const byte ascii_70[] PROGMEM = {0, 0}; // p NOT IMPLEMENTED YET
const byte ascii_71[] PROGMEM = {0, 0}; // q NOT IMPLEMENTED YET
const byte ascii_72[] PROGMEM = {0, 0}; // r NOT IMPLEMENTED YET
const byte ascii_73[] PROGMEM = {0, 0}; // s NOT IMPLEMENTED YET
const byte ascii_74[] PROGMEM = {0, 0}; // t NOT IMPLEMENTED YET
const byte ascii_75[] PROGMEM = {0, 0}; // u NOT IMPLEMENTED YET
const byte ascii_76[] PROGMEM = {0, 0}; // v NOT IMPLEMENTED YET
const byte ascii_77[] PROGMEM = {0, 0}; // w NOT IMPLEMENTED YET
const byte ascii_78[] PROGMEM = {0, 0}; // x NOT IMPLEMENTED YET
const byte ascii_79[] PROGMEM = {0, 0}; // y NOT IMPLEMENTED YET
const byte ascii_7a[] PROGMEM = {0, 0}; // z NOT IMPLEMENTED YET
const byte ascii_7b[] PROGMEM = {0, 0}; // { NOT IMPLEMENTED YET
const byte ascii_7c[] PROGMEM = {0, 0}; // | NOT IMPLEMENTED YET
const byte ascii_7d[] PROGMEM = {0, 0}; // } NOT IMPLEMENTED YET
const byte ascii_7e[] PROGMEM = {0, 0}; // ~ NOT IMPLEMENTED YET

const byte* fontTable[] = {
  ascii_20,
  ascii_21,
  ascii_22,
  ascii_23,
  ascii_24,
  ascii_25,
  ascii_26,
  ascii_27,
  ascii_28,
  ascii_29,
  ascii_2a,
  ascii_2b,
  ascii_2c,
  ascii_2d,
  ascii_2e,
  ascii_2f,
  ascii_30,
  ascii_31,
  ascii_32,
  ascii_33,
  ascii_34,
  ascii_35,
  ascii_36,
  ascii_37,
  ascii_38,
  ascii_39,
  ascii_3a,
  ascii_3b,
  ascii_3c,
  ascii_3d,
  ascii_3e,
  ascii_3f,
  ascii_40,
  ascii_41,
  ascii_42,
  ascii_43,
  ascii_44,
  ascii_45,
  ascii_46,
  ascii_47,
  ascii_48,
  ascii_49,
  ascii_4a,
  ascii_4b,
  ascii_4c,
  ascii_4d,
  ascii_4e,
  ascii_4f,
  ascii_50,
  ascii_51,
  ascii_52,
  ascii_53,
  ascii_54,
  ascii_55,
  ascii_56,
  ascii_57,
  ascii_58,
  ascii_59,
  ascii_5a,
  ascii_5b,
  ascii_5c,
  ascii_5d,
  ascii_5e,
  ascii_5f,
  ascii_60,
  ascii_61,
  ascii_62,
  ascii_63,
  ascii_64,
  ascii_65,
  ascii_66,
  ascii_67,
  ascii_68,
  ascii_69,
  ascii_6a,
  ascii_6b,
  ascii_6c,
  ascii_6d,
  ascii_6e,
  ascii_6f,
  ascii_70,
  ascii_71,
  ascii_72,
  ascii_73,
  ascii_74,
  ascii_75,
  ascii_76,
  ascii_77,
  ascii_78,
  ascii_79,
  ascii_7a,
  ascii_7b,
  ascii_7c,
  ascii_7d,
  ascii_7e
};

s_color color = {255, 0, 0};
s_color nextColor = {255, 0, 0};

bool tBlink = false;
bool nextBlink = false;

String text = "LED HAT\n";
String nextText = "LED HAT\n";

byte delayTime = 15;
byte lightValue = 50;

String serialInput;

/*
 * CMD:
 *  First byte ASCII =:
 *    change Text. Every following byte is interpreted as text
 *    
 *  First byte ASCII &:
 *    change Color. Next Byte is interpreted as Color index (0-3)
 *    
 *  First byte ASCII !:
 *    Blinking text
 *    
 *  First byte ASCII %:
 *    Second byte - 0x20 determine delay time => change speed
 */

#define CUR_CHAR ascii_66
ascii ascii_char;


CRGB leds[NUM_LEDS];

void setup() {
  FastLED.addLeds<NEOPIXEL, 6>(leds, NUM_LEDS);

  for( int i = 0; i < NUM_LEDS; i++ ) {
    leds[i].red = 0;
    leds[i].blue = 0;
    leds[i].green = 0;
  }
  FastLED.show();

  Serial.begin(9600);

}

void loop() {

  for( byte pos = 0; pos < 35; ++pos ) {

    displayText(pos, lightValue);

    /* 
     *  Serial receive with 10ms delay
     *  everytime only the next variables are set, because at this moment nothing is cleared on led screen
     *  update variable when everything is cleared before next draw
     */
    for( int i = 0; i < 10; ++i) {
      receive();
      delay(delayTime);
    }

    displayText(pos, 0);

    // Copy next text to current text variable
    /*for( int i = 0; i < 20; ++i ) {
      text[i] = nextText[i];
    }*/
    text = nextText;
    color = nextColor;
    tBlink = nextBlink;
  } 

  
}

void receive() {
  if( Serial.available() ) {
    char c = Serial.read();
    Serial.print(c, HEX);
    Serial.print(" ");
    Serial.println(c);

    // Ignore \r
    if( c == '\r' ) return;

    // Save char and increment pointer
    serialInput += c;
  
    // If byte is \n than end command
    if( c == '\n' ) {

      // Change Color
      if( serialInput[0] == '#' ) {

        // 3 Characters: & [COLOR] \n
        if( serialInput.length() == 3 ) {
          
          switch( serialInput[1] ) {
            // red
            case '0':
              nextColor.r = 255;
              nextColor.g = 0;
              nextColor.b = 0;
              break;

            // green
            case '1':
              nextColor.r = 0;
              nextColor.g = 255;
              nextColor.b = 0;
              break;

            // blue
            case '2':
              nextColor.r = 0;
              nextColor.g = 0;
              nextColor.b = 255;
              break;

            // Wrong parameter
            default:
              break;
          }
        }

        // & [HEX R] [HEX G] [HEX B] \n
        if( serialInput.length() == 8 ) {
          for( int i = 0; i < 3; ++i ) {
            // 0x30 = 0, calculate ascii char to number
            byte nibbleHigh = serialInput[1 + i * 2] - 0x30;
            byte nibbleLow = serialInput[2 + i * 2] - 0x30;

            if( nibbleHigh > 9 ) nibbleHigh -= 7;
            if( nibbleLow > 9 ) nibbleLow -= 7;

            // Combine high nibble and low nibble to one byte
            byte value = (nibbleHigh << 4) | nibbleLow;

             switch( i ) {
               case 0:
                 nextColor.r = value;
                 break;
                
               case 1:
                 nextColor.g = value;
                 break;

               case 2:
                 nextColor.b = value;
                 break;
             }
          }
        }
      }

  
       // Change Text
       if( serialInput[0] == '=' ) {
  
        // Copy Text from input buffer to next text with \n as end of string
        /*for( int j = 1; j < 21 && serialInput[j-1] != '\n'; ++j ) {
          nextText[j-1] = serialInput[j];
        }*/
        nextText = serialInput;
      }
  
      // Toggle Blink
      if( serialInput[0] == '!' ) {
        nextBlink = !nextBlink;
      }
  
      // Change speed
      if( serialInput[0] == '$' ) {
        delayTime = serialInput[1] - 0x20;
      } 

      if( serialInput[0] == '%' ) {

        // Last position before \n
        // Only take first 3 digits
        int to = serialInput.length() - 1;
        if( to > 4 ) {
          to = 4;
        }
        lightValue = serialInput.substring(1, to).toInt();  
      }
     
  
      serialInput = "";
    
    }
  }
}

void displayText(int pos, int value) {
  byte shifted = 0;
  
  // Loop Text on
  for( int i = 0; i < 20 && text[i] != '\n'; ++i ) {

    // Every second time break loop for blinking when in blinking mode
    if( tBlink && pos % 2 == 0 ) break;

    // In Blink mode change color 
    if( tBlink ) {
      color.r = 0;
      color.b = 0;
      color.g = 0;
      
      switch( pos % 3 ) {
        case 0:
          color.r = 255;
          break;

        case 1:
          color.g = 255;
          break;

        case 2:
          color.b = 255;
          break;
      }
    }

    // Skip non-Text ASCII
    if( text[i] < 0x20 || text[i] > 0x7e) {
      continue;
    }
      
    byte* ptr = fontTable[text[i] - 0x20];
    ascii_char.space = pgm_read_byte_near(ptr);
    ascii_char.size = pgm_read_byte_near(ptr+1);

    int offsetLetter = shifted - pos;
    if( offsetLetter < -12 ) {
      offsetLetter += 35;
    } else if( offsetLetter > 12 ) {
      offsetLetter -= 35;
    }
    
    for( int j = 0; j < ascii_char.size; ++j ) {
      int offsetLED = pgm_read_byte_near(ptr+j+2) + offsetLetter;
     
      leds[offsetLED].r = (color.r / 255.0) * (float) value;
      leds[offsetLED].g = (color.g / 255.0) * (float) value;
      leds[offsetLED].b = (color.b / 255.0) * (float) value;
    }
    shifted += ascii_char.space + 1; 
  }
  
  FastLED.show();
}
